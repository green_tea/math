//
// Created by ice-tea on 10.07.18.
//

#ifndef ALLMATH_BIGINTEGER_H
#define ALLMATH_BIGINTEGER_H

#include <vector>
#include <string>
#include <list>
#include <array>

class BigInteger {
private:
    const static size_t MAX_CHUNK_SIZE = 16;

    std::list <long long int> number;
    std::list <short int> chunkSize;
    std::array<long long, MAX_CHUNK_SIZE + 2> arr;

    void preCalcs();

    void pushFrontNumeral(short int numeral);

    size_t getLenNumeral(long long int numeral);

public:
    BigInteger(std::string s);
    BigInteger(long long int numeral);
    BigInteger();

    //just for a test
    void print();
};


#endif //ALLMATH_BIGINTEGER_H
