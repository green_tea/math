//
// Created by ice-tea on 10.07.18.
//

#include <iostream>
#include "bigInteger.h"

BigInteger::BigInteger() {
    preCalcs();
    chunkSize.clear();
    number.clear();
}

BigInteger::BigInteger(std::string s) : BigInteger() {
    if (s.empty()) return;
    for (int i = s.size() - 1; i >= 0; --i) {
        pushFrontNumeral(s[i] - '0');
    }
}

void BigInteger::preCalcs() {
    long long k = 1;
    for (int i = 0; i < MAX_CHUNK_SIZE + 2; ++i) {
        arr[i] = k;
        k *= 10;
    }
}

size_t BigInteger::getLenNumeral(long long int numeral) {
    size_t left = 0;
    size_t right = arr.size();
    while (right - left > 1) {
        size_t mid = (left + right) / 2;
        if (arr[mid] > numeral) right = mid;
        else left = mid;
    }
    return left;
}

BigInteger::BigInteger(long long int numeral) : BigInteger() {
    if (numeral > arr[MAX_CHUNK_SIZE + 1] - 1) {
        chunkSize.push_front(MAX_CHUNK_SIZE);
        chunkSize.push_front(1);
        number.push_front(numeral % arr[MAX_CHUNK_SIZE + 1]);
        number.push_front(numeral / arr[MAX_CHUNK_SIZE + 1]);
        return;
    }
    chunkSize.push_front(getLenNumeral(numeral));
    number.push_front(numeral);
}

// TODO сделать так, чтобы предподсчет степеней считался только 1 раз(не в конструкторе!!!)


void BigInteger::pushFrontNumeral(short int numeral) {
    if (chunkSize.empty()) {
        chunkSize.push_front(1);
        number.push_front(numeral);
        return;
    }
    short int size = chunkSize.front();
    if (size == MAX_CHUNK_SIZE) {
        chunkSize.push_front(1);
        number.push_front(numeral);
    }
    else {
        (*number.begin()) += numeral * arr[size];
        ++(*chunkSize.begin());
    }
}

// TODO дописать добавление промежутка



void BigInteger::print() {
    for (auto item : number) {
        std::cout << item;
    }
    std::cout << "\n";
}
