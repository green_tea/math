#include <iostream>
#include <algorithm>
#include "bigInteger.h"

int main() {
    BigInteger myInteger("123456");
    BigInteger mySecondIndeger(123456);
    BigInteger mythirdInteger("871236781254871253761278457821639812587351298647125639862174598216387851273587126398126736");
    BigInteger myfourthInteger(123456789012345678);
    myInteger.print();
    mySecondIndeger.print();
    mythirdInteger.print();
    myfourthInteger.print();
}